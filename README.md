# MXLDeferredKit
An iOS SDK for tracking deferred deep linking in iOS. Send a link to friends, and get rewarded when they install it.
