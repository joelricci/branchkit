//
//  RewardsViewController.h
//  BranchTest
//
//  Created by Joel on 3/2/15.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RewardsViewController : UIViewController

typedef enum : NSUInteger {
    Reward1 = 0,
    Reward2,
    Reward3,
    Reward4
} RewardType;

@property (weak, nonatomic) IBOutlet UILabel *lbCredits;
@property (weak, nonatomic) IBOutlet UIButton *btnIAP1;
@property (weak, nonatomic) IBOutlet UIButton *btnIAP2;
@property (weak, nonatomic) IBOutlet UIButton *btnIAP3;
@property (weak, nonatomic) IBOutlet UIButton *btnIAP4;

@end
