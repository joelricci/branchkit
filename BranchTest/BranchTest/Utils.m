//
//  Utils.m
//  BranchTest
//
//  Created by Joel on 3/3/15.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import "Utils.h"


void (^_alertActionHandler)(void);


@implementation Utils

+ (void)alertWithTitle:(NSString *)title message:(NSString *)message target:(UIViewController *)target handler:(void (^)(void))handler {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (!alert) {
        
        // legacy support
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        _alertActionHandler = handler;
        
        [alert show];
    }
    else {
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  if (handler) {
                                                                      handler();
                                                                  }
                                                              }];
        
        [alert addAction:defaultAction];
        
        [target presentViewController:alert animated:YES completion:nil];
    }
}

- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    //legacy alert view delegate
    
    if (_alertActionHandler) {
        _alertActionHandler();
    }
}

+ (void)alertError:(NSString *)message target:(UIViewController *)target {
    
    [self alertWithTitle:@"Sorry..." message:message target:target handler:^(void) {
        //nop
    }];
}

@end
