//
//  ViewController.m
//  BranchTest
//
//  Created by Joel Ricci on 15/02/03.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import "ViewController.h"
#import "RewardsViewController.h"
#import "MXLDeferredKit.h"    //include this for shareInvite convenience methods
#import "Utils.h"

@interface ViewController () {
    NSInteger _credits;
}

@property (nonatomic, readwrite) NSString *uid;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Loading user name from app cache to keep the UI updated. Branch automatically keeps track of every user session internally.
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uid = [defaults stringForKey:@"uid"];
    
    if (uid) {
        
        NSLog(@"UID loaded from cache");
        
        _uid = uid;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self updateUI];
    
}

- (void)updateUI {
    
    NSLog(@"Updating UI");
    
    self.textField.text = self.uid;
    self.lbCredits.text = @"Credits:";
    
    // Branch keeps track of the user's credits (if any). Use the following method to load current balance
    
    [MXLDeferredKit getCreditsWithCompletion:^(long credits, NSError *error) {
        
        if (error) {
            
            NSLog(@"Error while getting credits");
            
            return;
        }

        NSLog(@"got Credits = %ld", credits);
        
        _credits = credits;
        
        self.lbCredits.text = [NSString stringWithFormat:@"Credits: %ld", (long)_credits];
        self.btnRewards.hidden = NO;
        [self.activityIndicator stopAnimating];
    }];
    
    [self.activityIndicator startAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (self.textField.isFirstResponder) {
        [self.textField resignFirstResponder];
    }
}

- (IBAction)editingDidBegin:(id)sender {
}

- (IBAction)editingDidEnd:(id)sender {
    
    if (self.textField.text.length == 0) {
        self.textField.text = self.uid;
    }
    else {
        self.uid = self.textField.text;
    }
}

- (IBAction)editingDidEndOnExit:(id)sender {
}

- (IBAction)tapRefresh:(id)sender {
    
    [self updateUI];
}

- (IBAction)tapRewards:(id)sender {
    
    if (_credits == 0) {
        
        [Utils alertError:@"You don't have any credits. Share the app to gain credits!" target:self];
        
        return;
    }
    
    [self performSegueWithIdentifier:@"RewardsSegue" sender:nil];
}

- (IBAction)tapTestPopup:(id)sender {
    
    [MXLDeferredKit notifyReferredInstallWithTitle:@"What's up?" message:@"Thanks a bunch for joining your friend %@ on %@" referrer:@"Fooman" appName:@"Foobar" pictureURL:[NSURL URLWithString:@"https://s3.amazonaws.com/ksr/avatars/3075521/Avatar.small.jpg?1343813059"]];
    
    return;
    
}

- (IBAction)tapShare:(id)sender {
    
    if (!self.uid) {
        
        // Branch doesn't require a user-id to be set, but it lets us track referrers in the Branch dashboard.
        
        [Utils alertError:@"Input your name before sharing" target:self];
        
        return;
    }
    
    // Construct a share message
    NSString *shareMessage = [NSString stringWithFormat:@"%@ has invited you to try Reward Test from MobileXLabs!", self.uid];
    
    // Optional profile picture URL
    NSURL *profilePicture = [NSURL URLWithString:@"https://s3.amazonaws.com/ksr/avatars/3075521/Avatar.small.jpg?1343813059"];
    
    // Launch the Share Sheet
    [MXLDeferredKit shareInviteWithMessage:shareMessage appName:@"Test App" referrer:self.uid pictureURL:profilePicture];
}

- (void)setUid:(NSString *)uid {
    
    // This method sets Branch identity and needs only be done if uid is new or changed. Branch doesn't require user-id to be set but it allows us to track referrers.
    
    _uid = uid;
    
    NSLog(@"Setting UID to %@", self.uid);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:self.uid forKey:@"uid"];
    [defaults synchronize];
    
    // This registers the user-id which will be used to identify this session

    [MXLDeferredKit setIdentity:uid];
}

@end
