//
//  AppDelegate.h
//  BranchTest
//
//  Created by Joel Ricci on 15/02/03.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
