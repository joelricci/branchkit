//
//  main.m
//  BranchTest
//
//  Created by Joel Ricci on 15/02/03.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
