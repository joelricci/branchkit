//
//  RewardsViewController.m
//  BranchTest
//
//  Created by Joel on 3/2/15.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import "RewardsViewController.h"
#import "MXLDeferredKit.h"
#import "Utils.h"


@interface Reward : NSObject

@property (nonatomic, assign) int productType;
@property (nonatomic, assign) int value;
@property (nonatomic, assign) NSString *name;

- (id)initWithName:(NSString *)name andValue:(int)value;

@end

@implementation Reward

- (id)initWithName:(NSString *)name andValue:(int)value {
    
    self.name = name;
    self.value = value;
    return self;
}

@end

@interface RewardsViewController () {    
    long _credits;
    NSArray *_rewards;
}
@end

@implementation RewardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _rewards = @[
        [[Reward alloc] initWithName:@"Ice Cream" andValue:1],
        [[Reward alloc] initWithName:@"Tin Hat" andValue:5],
        [[Reward alloc] initWithName:@"Helmet" andValue:10],
        [[Reward alloc] initWithName:@"Scuba Gear" andValue:25],
    ];
}

- (void)viewWillAppear:(BOOL)animated {
    
    // Branch keeps track of the user's credits (if any). Use the following method to load current balance from the backend;
    
    [self refreshCredits];
}

- (void)refreshCredits {
    
    [MXLDeferredKit getCreditsWithCompletion:^(long credits, NSError *error) {
        
        if (error) {
            
            NSLog(@"loadRewards Error; %@", error.description);
            
            [Utils alertError:error.localizedDescription.description target:self];
        }
        else {
            
            NSLog(@"Credits: %ld", credits);
            
            [self setCreditsLabelTo:credits];
            
            _credits = credits;
        }
    }];
}

- (void)setCreditsLabelTo:(long)credits {
    self.lbCredits.text = [NSString stringWithFormat:@"Credits: %ld", credits];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)tapIAP1:(id)sender {
    [self redeem:Reward1];
}

- (IBAction)tapIAP2:(id)sender {
    [self redeem:Reward2];
}

- (IBAction)tapIAP3:(id)sender {
    [self redeem:Reward3];
}

- (IBAction)tapIAP4:(id)sender {
    [self redeem:Reward4];
}

- (void)redeem:(RewardType)rewardType {
    
    Reward *reward = _rewards[rewardType];
    
    long value = reward.value;
    NSString *name = reward.name;
    
    if (_credits < value) {
        
        [Utils alertError:[NSString stringWithFormat:@"You need at least %ld credits to buy %@", value, name] target:self];
        
        return;
    }
    
    // For Branch to keep track of the credit balance we need to tell the backend to redeem credits whenever the user makes a purchase.
    
    // update credit account on Branch backend
    [MXLDeferredKit redeemCredits:value];
    
    // Make sure that the credits has been updated on the backend. This is probably superflous!
    [MXLDeferredKit getCreditsWithCompletion:^(long credits, NSError *error) {

        if (_credits - value == credits) {
        
            _credits = credits;
            
            [MXLDeferredKit userCompletedAction:reward.name]; // Track event
            
            [Utils alertWithTitle:@"Congratulations" message:[NSString stringWithFormat:@"You got yourself %@ =)", name] target:self handler:nil];
            
            [self setCreditsLabelTo:credits];
        }
        else {
            [Utils alertError:@"Purchase failed. Please try again" target:self];
        }
    }];
}

@end
