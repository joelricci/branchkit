//
//  ViewController.h
//  BranchTest
//
//  Created by Joel Ricci on 15/02/03.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


- (IBAction)editingDidBegin:(id)sender;
- (IBAction)editingDidEnd:(id)sender;
- (IBAction)editingDidEndOnExit:(id)sender;
- (IBAction)tapRefresh:(id)sender;
- (IBAction)tapRewards:(id)sender;
- (IBAction)tapShare:(id)sender;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *uidView;
@property (weak, nonatomic) IBOutlet UILabel *lbCredits;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *btnRewards;



@end

