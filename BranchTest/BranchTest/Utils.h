//
//  Utils.h
//  BranchTest
//
//  Created by Joel on 3/3/15.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (void)alertWithTitle:(NSString *)title message:(NSString *)message target:(UIViewController *)target handler:(void (^)(void))handler;

+ (void)alertError:(NSString *)message target:(UIViewController *)target;

@end
