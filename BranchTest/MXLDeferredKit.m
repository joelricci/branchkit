//
//  MXLDeferredKit.m
//  BranchTest
//
//  Created by Joel on 3/12/15.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import "MXLDeferredKit.h"
#import "Branch.h"

// Fallback URL required by Branch:
static NSString *const MXLDefaultURL = @"http://mobilexlabs.com";

// Constants used to identify deep link parameters
static NSString *const MXLReferrerID = @"MXLReferrer";
static NSString *const MXLPictureURLID = @"MXLPictureURL";
static NSString *const MXLAppNameID = @"MXLAppName";

@interface MXLDeferredKit ()

@property (nonatomic, assign) BOOL isBranchInitCalled;

@end

@implementation MXLDeferredKit

+(void)enableDebugMode:(BOOL)debug {
    
    if ([MXLDeferredKit sharedInstance].isBranchInitCalled) {
        NSLog(@"MXLDeferredKit Error; Debug mode must be set before initializing the MXLDeferredKit!");
    }
    
    if (debug) {
        NSLog(@"MXLDeferredKit: Debug mode is active. Disable this before distributing the App!");
        [Branch setDebug];
    }
}

+ (void)initWithLaunchOptions:(NSDictionary *)launchOptions messageTitle:(NSString *)title messageFormat:(NSString *)format {
    
    Branch *branch = [Branch getInstance];
    
    [branch initSessionWithLaunchOptions:launchOptions /* isReferrable:YES */ andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        
        if (error) {
            NSLog(@"MXLDeferredKit Error; Branch init returned with error: %@", error.localizedDescription.description);
            return;
        }
        
        // params contains the deep link data if app was installed or launched from a URL
        
        NSURL *pictureURL =[NSURL URLWithString:params[MXLPictureURLID]];
        NSString *referrer = params[MXLReferrerID];
        NSString *appName = params[MXLAppNameID];

        if (referrer) {
            
            NSLog(@"MXLDeferredKit: Referred Install detected");
            
            // We have a referred install! Let's take appropriate action
            
            NSString *actualTitle = title;
            NSString *messageTemplate = format;
            
            if (!actualTitle) {
                actualTitle = @"Hi there!";
            }

            if (!messageTemplate) {
                messageTemplate = @"Thanks for joining your friend %@ on %@";
            }
            
            [self notifyReferredInstallWithTitle:actualTitle message:messageTemplate referrer:referrer appName:appName pictureURL:pictureURL];
        }
        
        NSLog(@"MXLDeferredKit: Branch init done");
    }];
    
    [MXLDeferredKit sharedInstance].isBranchInitCalled = YES;
}

+ (BOOL)handleDeeplink:(NSURL *)url{
    
    return [[Branch getInstance] handleDeepLink:url];
}

+ (void)shareInviteWithMessage:(NSString *)message appName:(NSString *)appName referrer:(NSString *)referrer pictureURL:(NSURL *)url{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:@"invite" forKey:@"feature"];
    
    if (referrer) {
        params[MXLReferrerID] = referrer;
    }
    
    if (url) {
        params[MXLPictureURLID] = url.absoluteString;
    }
    
    if (appName) {
        params[MXLAppNameID] = appName;
    }
    else {
        NSLog(@"MXLDeferredKit WARNING; shareInviteWithMessage: appName should not be nil!");
    }
    
    // Branch UIActivityItemProvider
    UIActivityItemProvider *itemProvider = [Branch getBranchActivityItemWithDefaultURL:MXLDefaultURL andParams:params];
    
    // Pass this in the NSArray of ActivityItems when initializing a UIActivityViewController
    UIActivityViewController *shareViewController = [[UIActivityViewController alloc] initWithActivityItems:@[message, itemProvider] applicationActivities:nil];
    
    // Get the current View Controller from the view stack
    UIViewController *currentVC = [self getTopmostController];
    
    if ([shareViewController respondsToSelector:@selector(popoverPresentationController)]) {
        
        // On ios8 the ipad will use a popoverPresentationController to display the activity view, which requires an anchor point.
        
        // This is hackish but since we don't have a reference to the view that is triggering the popover, let's just create an anchor point at the bottom center of the screen:
        
        UIView *anchor = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        anchor.center = CGPointMake(currentVC.view.frame.size.width/2, currentVC.view.frame.size.height);
        
        [currentVC.view addSubview:anchor];
        
        shareViewController.popoverPresentationController.sourceView = anchor;
    }

    // Present the share sheet!
    [currentVC presentViewController:shareViewController animated:YES completion:^{
        
        // Let's signal a custom user events to provide stats that allows us to create conversion funnels and other neato tricks to track user behaviour inside our app.
        
        [[Branch getInstance] userCompletedAction:@"shared"];
    }];
}

// Get the current View Controller from the view stack:

+ (UIViewController*)getTopmostController {
    
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    // Traverse the VC stack
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

+ (void)notifyReferredInstallWithTitle:(NSString *)title message:(NSString *)template referrer:(NSString *)referrer appName:(NSString *)appName pictureURL:(NSURL *)url {

    UIView *topView = [self getTopmostController].view;

    NSString *message = [NSString stringWithFormat:template, referrer, appName];
    
    MXLReferrerView *referrerView = [[MXLReferrerView alloc] initWithTitle:title message:message pictureURL:url];
    referrerView.layer.opacity = 0;
    referrerView.center = CGPointMake(topView.frame.size.width/2, topView.frame.size.height * 0.49);

    UIView *dimmer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, topView.frame.size.width, topView.frame.size.height)];
    dimmer.backgroundColor = [UIColor blackColor];
    dimmer.layer.opacity = 0;
    
    referrerView.dimmerView = dimmer;
    
    [topView addSubview:dimmer];
    [topView addSubview:referrerView];
    
    [UIView animateWithDuration:0.4 animations:^{
        dimmer.layer.opacity = 0.3;
        referrerView.center = CGPointMake(topView.frame.size.width/2, topView.frame.size.height/2);
        referrerView.layer.opacity = 1.0;
    }];
}

+ (void)redeemCredits:(long)value {
    
    // update credit account on Branch backend
    [[Branch getInstance] redeemRewards:value];
}

+ (void)getCreditsWithCompletion:(void(^)(long credits, NSError *error))block {
    
    [[Branch getInstance] loadRewardsWithCallback:^(BOOL changed, NSError *error) {
        // changed boolean will indicate if the balance changed from what is currently in memory
        
        // will return the balance of the current user's credits
        NSInteger credits = [[Branch getInstance] getCredits];
        
        block(credits, error);
    }];
    
}

+ (void)setIdentity:(NSString *)userId {
    
    [[Branch getInstance] setIdentity:userId];
}

+ (void)userCompletedAction:(NSString *)action {
    
    [[Branch getInstance] userCompletedAction:action];
}

+ (NSString *)getReferrerIdentity {
    
    NSDictionary *sessionParams = [[Branch getInstance] getLatestReferringParams];
    
    return sessionParams[MXLReferrerID];
}

+ (NSURL *)getReferrerPictureURL {
    
    NSDictionary *sessionParams = [[Branch getInstance] getLatestReferringParams];
    
    return sessionParams[MXLPictureURLID];
}

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    
    self = [super init];
    
    if (!self) {
        return nil;
    }

    _isBranchInitCalled = NO;
    
    return self;
}

@end


@interface MXLReferrerView ()

@end

@implementation MXLReferrerView

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message pictureURL:(NSURL *)url {
    
    self = [super init];
    
    if (!self) {
        
        return nil;
    }

    return [self newReferrerViewWithTitle:title message:message pictureURL:url];
}

- (instancetype)newReferrerViewWithTitle:(NSString *)title message:(NSString *)message pictureURL:(NSURL *)url {
    
    // Construct Popup box
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    CGFloat width, height;
    CGRect  avatarRect;
    
    if ((screenSize.width/screenSize.height) < 1.0f) {
        
        // Define a portrait shaped Popup box:
        
        height = MIN(screenSize.height * 0.45, 300);
        width = height * 0.9;
        
        // Make avatar a square box at the top of Popup box:
        
        avatarRect.origin = CGPointZero;
        avatarRect.size.width = MIN(width * 0.4, 150);
        avatarRect.size.height = MIN(width * 0.4, 150);
    }
    else {
        
        // Define a landscape shape Popup box:
        
        height = MIN(screenSize.height * 0.7, 300);
        width = height * 1.0;
        
        // Make avatar a square box to the left inside the Popup box:
        
        avatarRect.origin = CGPointZero;
        avatarRect.size.width = MIN(width * 0.4, 150);
        avatarRect.size.height = MIN(width * 0.4, 150);
    }
    
    // Create the Popup box:
    
    [self setBounds:CGRectMake(0, 0, width, height)];
//    [self.layer setShadowColor:[UIColor blackColor].CGColor];
//    [self.layer setShadowOpacity:0.5];
    
    // Create avatar view:
    
    UIImageView *avatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, avatarRect.size.width, avatarRect.size.height)];
    
    NSData *imgData = [NSData dataWithContentsOfURL:url];
    
    if (imgData) {
        
        NSLog(@"MXLDeferredKit: image data loaded from URL");
        
        avatar.image = [UIImage imageWithData:imgData];
        avatar.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        avatar.layer.cornerRadius = avatar.layer.frame.size.width/2;
        avatar.layer.masksToBounds = YES;
        avatar.center = CGPointMake(self.frame.size.width/2, 0);
        
        [self addSubview:avatar];
    }
    else {
        NSLog(@"MXLDeferredKit Warning; failed to load image data from URL %@", url);
    }
    
    // Create title just below the avatar frame:
    
    CGRect titleLabelRect = CGRectMake(0, avatar.frame.size.height/2, width, height * 0.25);
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:height/12];
    
    // Create message:
    
    UIEdgeInsets inset = UIEdgeInsetsMake(0, 10, 0, 10);  //padding
    
    CGRect messageLabelRect = CGRectMake(0, titleLabel.frame.origin.y+titleLabel.frame.size.height, width, height * 0.3);
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:UIEdgeInsetsInsetRect(messageLabelRect, inset)];
    messageLabel.text = message;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:height/15];
    messageLabel.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1.0];
//    messageLabel.adjustsFontSizeToFitWidth = YES;
//    messageLabel.minimumScaleFactor = 0.1;
//    messageLabel.backgroundColor = [UIColor redColor];
    
    // Button:
    
    CGRect buttonRect = CGRectMake(0, height * 0.8, width, height * 0.2);
    
    UIButton *button = [[UIButton alloc] initWithFrame:buttonRect];
    [button addTarget:self action:@selector(tappedPopupButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"OK" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.3];
    
    // Finalize view:
    
    [self addSubview:titleLabel];
    [self addSubview:messageLabel];
    [self addSubview:button];
    [self setBackgroundColor: [UIColor whiteColor]];
    
    return self;
}

- (void)tappedPopupButton:(id)sender {
    
    NSLog(@"MXLReferrerView dismissed");
    
    [UIView animateWithDuration:0.4 animations:^{
        self.dimmerView.layer.opacity = 0;
    } completion:^(BOOL finished) {
        [self.dimmerView removeFromSuperview];
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.center = CGPointMake(self.center.x, self.center.y * 0.95);
        self.layer.opacity = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end

