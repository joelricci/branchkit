//
//  MXLDeferredKit.h
//  BranchTest
//
//  Created by Joel on 3/12/15.
//  Copyright (c) 2015 Joel Ricci. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MXLDeferredKit : NSObject

//@property (nonatomic, readonly) UIImage *profilePicture;      TODO
//@property (nonatomic, readonly) NSString *referrerName;       TODO

/**
 *  Enables Branch debug mode when set to YES. This must be done before initializing MXLDeferredKit! When debug mode is enabled a device id will not be remembered so that a referred install will be registered every time the app is launched from a shared link. This is very handy for testing your app but remember to disable debug mode before distributing your App!
 *
 *  @param debug Enables Branch debug mode when set to YES. This must be done before initializing MXLDeferredKit!
 */
+ (void)enableDebugMode:(BOOL)debug;

/**
 *  Initializes the kit. This will set up a session with Branch and handle any referred installs. This method should be called from the App delegate using the launchOptions as the first argument. Make sure that the App's Branch key (bnc_app_key) has been added to the App's Info.plist.
 *
 *  @param launchOptions Launch options from the App delegate.
 *  @param title         The title of the referred install popup, e.g "Hi there!". If nilled, a default title will be used.
 *  @param messageFormat The message template of the popup as a formatted string with one or two placeholders; the first will be substituted with the name of the referrer and the second (optional) with the name of the App. E.g "Thanks for joining your friend %@ on %@!". If nilled, a default template will be used.
 */
+ (void)initWithLaunchOptions:(NSDictionary *)launchOptions messageTitle:(NSString *)title messageFormat:(NSString *)format;

/**
 *  (Optional) Checks if the App has been opened as a result of the user executing a deep link. It should be implemented in the App delegate's -application:openURL:sourceApplication:annotation method by checking its return value. If the value is true (YES) it should route tha App to the target. For this method to work, a custom URL Type has to be configured in the App's Info.plist settings as the first item under URL Types.
 *
 *  @param url The url to be opened, provided by -application:openURL:sourceApplication:annotation
 *
 *  @return Returns true (YES) if the App has been deep linked and should be routed to the corresponding destination.
 */
+ (BOOL)handleDeeplink:(NSURL *)url;

/**
 *  @return the Identity of the referrer, if any, or nil.
 */
+ (NSString *)getReferrerIdentity;

/**
 *  @return the picture URL of the referrer, if any, or nil.
 */
+ (NSURL *)getReferrerPictureURL;

/**
 *  Constructs and displays a ActivityViewController known as a "Share sheet" which lets the user choose a sharing action from a popup dialog. A unique Branch link is automatically created and shared through the selected channel.
 *
 *  @param message  The message to be shared, e.g: "Your friend Foogirl has invited you to join UltraApp"
 *  @param appName  The name of the App, e.g "Angry Bees"
 *  @param referrer The name of the referring (current) user, e.g "Foogirl"
 *  @param url      The profile / avatar picture URL of the current user, e.g. "http://somecdn.com/xyz/profile_thumbnail.jpg"
 */
+ (void)shareInviteWithMessage:(NSString *)message appName:(NSString *)appName referrer:(NSString *)referrer pictureURL:(NSURL *)url;

/**
 *  Redeems (consume) credits from the user's Branch credit depot. Use this to update the balance when the user consumes credits, such as buying an IAP.
 *
 *  @param value Number of credits to redeem
 */
+ (void)redeemCredits:(long)value;

/**
 *  Fetches the Branch credit balance for the current user.
 *
 *  @param block The completion block will be called with (long)credits and (NSError*)error.
 */
+ (void)getCreditsWithCompletion:(void(^)(long credits, NSError *error))block;

/**
 *  (Optional) Sets the User Identity associated with this session. This only needs to be done once per install as Branch automatically caches the id.
 *
 *  @param userId String of max 127 characters
 */
+ (void)setIdentity:(NSString *)userId;

/**
 *  Signals that the user has completed a custom action. This is used to track user behaviour inside your App, such as buying an IAP or reaching a certain level, etc. The action can be tracked in the Branch dashboard and used to create Conversion Funnels
 *
 *  @param action String describing the action, e.g: "Bought IAP1", "Level 10"
 */
+ (void)userCompletedAction:(NSString *)action;

/**
 *  (Internal) Displays the Referred Install popup view. This method should normally not be used as it's handled automatically by the DeferredKit but can be used to quickly test your message strings.
 *
 *  @param title    The title of the referred install popup, e.g "Hi there!". If nilled, a default title will be used.
 *  @param message  The message template of the popup as a formatted string with one or two placeholders; the first will be substituted with the name of the referrer and the second (optional) with the name of the App. E.g "Thanks for joining your friend %@ on %@!". If nilled, a default template will be used.
 *  @param referrer Name of the referring user, e.g "Foogirl"
 *  @param appName  The name of the App, e.g "Angry Bees"
 *  @param url      The profile / avatar picture URL of the current user, e.g. "http://somecdn.com/xyz/profile_thumbnail.jpg"
 */
+ (void)notifyReferredInstallWithTitle:(NSString *)title message:(NSString *)message referrer:(NSString *)referrer appName:(NSString *)appName pictureURL:(NSURL *)url;

/**
 *  (Internal) Utility function to get the topmost (current) view controller from the App's view stack.
 *
 *  @return The topmost (current) view controller
 */
+ (UIViewController*)getTopmostController;

@end

/**
 Constructs a Referred Install view used by MXLDeferredKit. Not supposed to be instantiated directly.
 */
@interface MXLReferrerView : UIView

@property (nonatomic, assign) UIView *dimmerView;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message pictureURL:(NSURL *)url;
- (void)tappedPopupButton:(id)sender;

@end